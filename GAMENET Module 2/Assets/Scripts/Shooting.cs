using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;
    public GameObject killListPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    private bool dead;
    public Image healthBar;

    public TextMeshProUGUI nameText;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        nameText.text = photonView.Owner.NickName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        if (Physics.Raycast(ray, out hit, 200))
        {   
            photonView.RPC("CreateHitEffects", RpcTarget.AllBuffered, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0 && dead == false)
        {
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            GameObject parent = GameObject.Find("Content");
            
            GameObject killItem = Instantiate(killListPrefab);
            killItem.transform.SetParent(parent.transform);
            killItem.transform.localScale = Vector3.one;

            killItem.transform.Find("Player1Text").GetComponent<Text>().text = info.Sender.NickName;
            killItem.transform.Find("Player2Text").GetComponent<Text>().text = info.photonView.Owner.NickName;

            info.Sender.kills += 1;

            Destroy(killItem, 5.0f);

            dead = true;

            if (info.Sender.kills == 10)
            {
                photonView.RPC("Win", RpcTarget.AllBuffered, info.Sender.NickName);
            }
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine("RespawnCountdown");
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText =  GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are Killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        dead = false;
        respawnText.GetComponent<Text>().text = "";
        this.transform.position = GameManager.instance.spawnPoints[Random.Range(0, 4)];
        transform.GetComponent<PlayerMovementController>().enabled = true;

        health = 100;
        healthBar.fillAmount = health / startHealth;
        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
        Debug.Log("finish");
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void Win(string name)
    {
        GameObject winText =  GameObject.Find("Win Text");
        winText.GetComponent<Text>().text = name + " Won the Game!";
        transform.GetComponent<PlayerMovementController>().enabled = false;
        StartCoroutine("ReturnLobby");
    }
    
    IEnumerator ReturnLobby()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
