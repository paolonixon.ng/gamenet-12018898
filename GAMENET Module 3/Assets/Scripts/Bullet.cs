using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && !other.gameObject.GetComponent<PhotonView>().IsMine)
        {
            other.gameObject.GetComponent<PhotonView>().RPC("TakeDamage",RpcTarget.AllBuffered, 10);
            Debug.Log("trigger");
            Destroy(this.gameObject);
        }
    }

    public void AddForce(Vector3 amm)
    {
        this.AddForce(amm * 5000);
    }
}
