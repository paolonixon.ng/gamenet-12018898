﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class WinController : MonoBehaviourPunCallbacks
{
    public static WinController WC = null;

    int deathcount;
    void Awake()
    {
        if(WC == null)
        {
            WC = this;
        }
    }
    public enum RaiseEventCode
    {
        WhoWonEventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventCode.WhoWonEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;
            string nickNameOfFinishedPlayer = (string) data[0];
            deathOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinishedPlayer + " " + deathOrder);
        
            GameObject orderUiText = DeathRaceManager.instance.deathTextUi[0];
            GameObject winUiText = DeathRaceManager.instance.deathTextUi[1];
            orderUiText.SetActive(true);
            winUiText.SetActive(true);

            if(viewId == photonView.ViewID && deathOrder < PhotonNetwork.CurrentRoom.PlayerCount && deathOrder != PhotonNetwork.CurrentRoom.PlayerCount)
            {
                orderUiText.GetComponent<Text>().text = nickNameOfFinishedPlayer + "(YOU) were eliminated";
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else if (deathOrder < PhotonNetwork.CurrentRoom.PlayerCount && deathOrder != PhotonNetwork.CurrentRoom.PlayerCount)
            {
                orderUiText.GetComponent<Text>().text = nickNameOfFinishedPlayer + " were eliminated";
                StartCoroutine("ClearTxt", orderUiText);
            }
        }
    }

    IEnumerator ClearTxt(GameObject UI)
    {
        yield return new WaitForSecondsRealtime(5.0f);
        UI.GetComponent<Text>().text = " ";
    }

    private int deathOrder = 0;

    public void OnDeath()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        
        deathOrder++;
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nickName, deathOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoWonEventCode, data, raiseEventOptions, sendOptions);
    }

    public void SetDeathCount(int deaths)
    {
        deathcount = deaths;
    }

    [PunRPC]
    public void Win()
    {
        StartCoroutine("ReturnLobby");
    }

    IEnumerator ReturnLobby()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
