﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

[RequireComponent(typeof(LineRenderer))]
public class Shooting : MonoBehaviourPunCallbacks
{
    public GameObject Barrel;
    public GameObject killListPrefab;
    public Rigidbody bullet;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    private bool dead;

    private int destroyed;
    public Image healthBar;

    public TextMeshProUGUI nameText;
    private Animator animator;
    public float gunRange = 200f;
    public float fireRate = 1f;
    public float laserDuration = 0.1f;

    public float gunFireRate = 0.5f;

    LineRenderer laserLine;
    float fireTimer;
    
    float gunFireTimer;

    //public WinController wc;
    void Awake()
    {
        laserLine = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        nameText.text = photonView.Owner.NickName;
    }

    // Update is called once per frame
    void Update()
    {
        fireTimer += Time.deltaTime;
        gunFireTimer += Time.deltaTime;
        if(photonView.IsMine)
        {
            FireLaser();
            FireGun();
        }
    }

    public void FireLaser()
    {
        if(Input.GetButtonDown("Fire1") && fireTimer > fireRate)
        {
            fireTimer = 0;
            laserLine.SetPosition(0, Barrel.transform.position);
            RaycastHit hit;
            if (Physics.Raycast(Barrel.transform.position, Barrel.transform.forward, out hit, gunRange))
            {   
                laserLine.SetPosition(1, hit.point);

                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 20);
                }
                else
                {
                    laserLine.SetPosition(1, Barrel.transform.position + (Barrel.transform.forward * gunRange));
                }
            }
            CreateLaserEffects(laserLine.GetPosition(1));
        }
    }

    public void FireGun()
    {
        if(Input.GetButtonDown("Fire2") && gunFireTimer > gunFireRate)
        {
            gunFireTimer = 0;
            var firedBullet = PhotonView.Instantiate(bullet, Barrel.transform.position, Barrel.transform.rotation);
            firedBullet.AddForce(Barrel.transform.forward * 5000, 0);
            Destroy(firedBullet.gameObject, 5.0f);
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        Debug.Log(photonView.Owner.NickName + " " + health + "HP");

        if (health <= 0 && dead == false)
        {
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            GameObject parent = GameObject.Find("Content");
            
            GameObject killItem = Instantiate(killListPrefab);
            killItem.transform.SetParent(parent.transform);
            killItem.transform.localScale = Vector3.one;

            killItem.transform.Find("Player1Text").GetComponent<Text>().text = info.Sender.NickName;
            killItem.transform.Find("Player2Text").GetComponent<Text>().text = info.photonView.Owner.NickName;

            destroyed += 1;

            Destroy(killItem, 5.0f);

            dead = true;
            if(destroyed == PhotonNetwork.CurrentRoom.PlayerCount - 1 && PhotonNetwork.CurrentRoom.PlayerCount != 1)
            {
                GameObject winUiText = DeathRaceManager.instance.deathTextUi[1];
                winUiText.SetActive(true);

                winUiText.GetComponent<Text>().text = info.Sender.NickName + " won the game!";
                Debug.Log(info.Sender.NickName + " won the game!");
                winUiText.GetComponent<Text>().color = Color.blue;
                GetComponent<VehicleMovement>().enabled = false;
                StartCoroutine("ReturnLobby");
            }

            Die();
        }
    }

    [PunRPC]
    public void CreateLaserEffects(Vector3 hit)
    {

        StartCoroutine("ShootLaser", hit);
    }

    IEnumerator ShootLaser(Vector3 hit)
    {
        laserLine.SetPosition(0, Barrel.transform.position);
        laserLine.SetPosition(1, hit);
        laserLine.enabled = true;
        yield return new WaitForSeconds(laserDuration);
        laserLine.enabled = false;
    }

    public void Die()
    {
        WinController.WC.OnDeath();
    }

    [PunRPC]
    public void Win(string name)
    {
        // GameObject winUiText = DeathRaceManager.instance.deathTextUi[1];
        // winUiText.SetActive(true);

        // winUiText.GetComponent<Text>().text = name + " won the game!";
        // Debug.Log(name + " won the game!");
        // winUiText.GetComponent<Text>().color = Color.blue;
        // GetComponent<VehicleMovement>().enabled = false;
        // StartCoroutine("ReturnLobby");
    }
    
    IEnumerator ReturnLobby()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
