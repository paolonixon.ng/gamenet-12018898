﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

public class Shoot : MonoBehaviourPunCallbacks
{
    public GameObject Barrel;
    public GameObject killListPrefab;
    public Rigidbody bullet;

     [Header("HP Related Stuff")]
    public float startHealth = 1;
    private float health;
    private bool dead;

    public float firerate;
    float firetimer;

    private int destroyed;
    // Start is called before the first frame update
    void Start()
    {
        firetimer += Time.deltaTime;
        health = startHealth;
    }

    // Update is called once per frame
    void Update()
    {
        firetimer += Time.deltaTime;
        if(photonView.IsMine)
        {
            Fire();
        }

        if (health <= 0 && dead == true)
        {
            dead = false;
        }
    }

    public void Fire()
    {
        if(Input.GetButtonDown("Fire1") && firetimer > firerate)
        {
            firetimer = 0;
            var firedBullet = PhotonNetwork.Instantiate("bullet", Barrel.transform.position, Barrel.transform.rotation);
            Debug.Log("Bang");
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;

        if (health <= 0 && dead == false)
        {
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            GameObject parent = GameObject.Find("Content");
            
            GameObject killItem = Instantiate(killListPrefab);
            killItem.transform.SetParent(parent.transform);
            killItem.transform.localScale = Vector3.one;

            killItem.transform.Find("Player1Text").GetComponent<Text>().text = info.Sender.NickName;
            killItem.transform.Find("Player2Text").GetComponent<Text>().text = info.photonView.Owner.NickName;
            if(info.Sender.NickName != info.photonView.Owner.NickName)
            {
                destroyed += 1;
            }

            StartCoroutine("Respawn");

            Destroy(killItem, 5.0f);

            KillController.KC.OnDeath();
            dead = true;
            if(destroyed >= 10)
            {
                GameObject winUiText = GameManager.instance.deathTextUi[1];
                winUiText.SetActive(true);

                winUiText.GetComponent<Text>().text = info.Sender.NickName + " won the game!";
                Debug.Log(info.Sender.NickName + " won the game!");
                winUiText.GetComponent<Text>().color = Color.blue;
                GetComponent<VehicleMovement>().enabled = false;
                StartCoroutine("ReturnLobby");
            }

        }
    }

    IEnumerator Respawn()
    {
        this.GetComponent<VehicleMovement>().enabled = false;
        this.GetComponent<Shoot>().enabled = false;
        yield return new WaitForSeconds(5.0f);
        this.GetComponent<VehicleMovement>().enabled = photonView.IsMine;
        this.GetComponent<Shoot>().enabled = photonView.IsMine;
        this.transform.position = GameManager.instance.startingPositions[Random.Range(0, GameManager.instance.startingPositions.Length)].position;
        health = startHealth;
        dead = false;
    }

    IEnumerator ReturnLobby()
    {
        yield return new WaitForSecondsRealtime(5.0f);
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
