﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    Camera camera;
    public GameObject Turret;
    public float speed = 20;
    public float rotationSpeed = 200;
    public float currentSpeed = 0;

    public bool isControlEnabled;

    public float TurretSpeed;
    float TurretAngle;

    void Start()
    {
        isControlEnabled = false;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;

        transform.Translate(0, 0, translation);
        currentSpeed = translation;

        if(translation != 0)
        {
            transform.Rotate(0, rotation, 0);
        }
        
        Look();
    }

    void FaceMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToViewportPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        //Turret.transform.Rotate(direction);

        Vector3 distance = Input.mousePosition - Turret.transform.position;
        Quaternion rotation = Quaternion.LookRotation(distance, Vector3.up);
        Turret.transform.rotation = Quaternion.Euler(0,0,rotation.z);
    }

    void Look()
    {
        Ray cameraRay = camera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;
        if(groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);
            Turret.transform.LookAt(new Vector3(pointToLook.x, Turret.transform.position.y, pointToLook.z));
        }
    }
}
