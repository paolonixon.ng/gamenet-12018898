﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
    public Rigidbody RGBBullet;
    public int NumOfBounce;

    private Vector3 lastVelocity;
    private float currentSpeed;
    private Vector3 direction;
    private int currBounces = 0;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 15.0f);
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 5000, 0);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        lastVelocity = RGBBullet.velocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag != "Player")
        {
            if(currBounces >= NumOfBounce)
            {
                Destroy(gameObject);
            }
            currentSpeed = lastVelocity.magnitude;
            direction = Vector3.Reflect(lastVelocity.normalized, collision.contacts[0].normal);
            RGBBullet.velocity = direction * Mathf.Max(currentSpeed, 0);
            currBounces++;
        }
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PhotonView>().RPC("TakeDamage",RpcTarget.AllBuffered, 1);
            Destroy(this.gameObject);
        }
    }

}
