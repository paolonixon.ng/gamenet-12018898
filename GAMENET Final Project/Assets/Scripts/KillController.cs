﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class KillController : MonoBehaviourPunCallbacks
{
    public static KillController KC = null;

    int deathcount;
    void Awake()
    {
        if(KC == null)
        {
            KC = this;
        }
    }
    public enum RaiseEventCode
    {
        WhoWonEventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventCode.WhoWonEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;
            string nickNameOfFinishedPlayer = (string) data[0];
            int viewId = (int)data[1];

            Debug.Log(nickNameOfFinishedPlayer + " " + deathOrder);
        
            GameObject orderUiText = GameManager.instance.deathTextUi[0];
            orderUiText.SetActive(true);


            orderUiText.GetComponent<Text>().text = nickNameOfFinishedPlayer + " were eliminated";
            StartCoroutine("ClearTxt", orderUiText);

        }
    }

    IEnumerator ClearTxt(GameObject UI)
    {
        yield return new WaitForSecondsRealtime(5.0f);
        UI.GetComponent<Text>().text = " ";
    }

    private int deathOrder = 0;

    public void OnDeath()
    {
        
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nickName, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoWonEventCode, data, raiseEventOptions, sendOptions);
    }
}
